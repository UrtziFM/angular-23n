import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { IFormValue } from './../../interfaces/reactive.interface';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {
  reactiveForm: FormGroup;
  submissions: IFormValue[];

  // Inject a FormBuilder instance in the class so we can use it's methods
  constructor(private formBuilder: FormBuilder) {
    this.reactiveForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      conditions: [false]
    });

    this.submissions = [];
  }

  ngOnInit() {}

  submitForm(value: IFormValue) {
    this.submissions.push(value);
    this.reactiveForm.reset();
  }
}
