import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { IHeroCharacter } from '../../../interfaces/hero.interface';
import { HeroService } from '../../../services/hero.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  heroForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private heroService: HeroService) {
    this.heroForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      picture: ['']
    });
  }

  ngOnInit() {}

  submitForm(value: IHeroCharacter): void {
    this.heroService.setHero(value);
  }
}
