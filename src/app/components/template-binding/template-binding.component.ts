import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-binding',
  templateUrl: './template-binding.component.html',
  styleUrls: ['./template-binding.component.scss']
})
export class TemplateBindingComponent implements OnInit {
  firstInputText: string;
  count: number;

  constructor() {
    this.firstInputText = 'Default value, change the input now ⌨';
    this.count = 0;
  }

  ngOnInit() {}

  // Input handler
  changeInputText(value: string): void {
    console.log('pressed value');
    this.firstInputText = value;
  }

  // Button handler
  addOne(): void {
    this.count += 1;
  }
}
